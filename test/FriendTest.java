import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FriendTest {
	private Customer jim;
	private Customer mik;
	private Customer crista;
	private static final long LOCAL_RATE = 3;
	private static final long LONG_DISTANCE_RATE = 10;
	
	@Before
	public void setUp() throws Exception {
		jim = new Customer("Jim", 650);
    	mik = new Customer("Mik", 650);
    	crista = new Customer("Crista", 415);
	}

	@Test
	public void testSetFriend_hasFriend() {
		jim.setFriend(crista);
		assertFalse("crista has been set as a jim's friend, even though she has a different area code", jim.hasFriend(crista));
		jim.setFriend(mik);
		assertTrue("mik has not been set as a jim's friend", jim.hasFriend(mik));
	}

	@Test
	public void testLocalCall() {
        Call c1 = jim.call(mik);
        mik.pickup(c1);
        wait(2);
        mik.hangup(c1);
        assertTrue(jim.totalCharge == 2*LOCAL_RATE);
        assertTrue(mik.totalCharge == 0);
        assertTrue(jim.totalConnectTime == 2);
        assertTrue(mik.totalConnectTime == 2);
	}
	
	@Test
	public void testLongDistanceCall() {
        Call c1 = jim.call(crista);
        crista.pickup(c1);
        wait(1);
        jim.hangup(c1);
        assertTrue(jim.totalCharge == LONG_DISTANCE_RATE);
        assertTrue(crista.totalCharge == 0);
        assertTrue(jim.totalConnectTime == 1);
        assertTrue(crista.totalConnectTime == 1);
	}
	
	@Test
	public void testCallFriend() {
        Call c1 = jim.call(mik);
        mik.pickup(c1);
        wait(1);
        mik.hangup(c1);
        assertTrue(jim.totalCharge == LOCAL_RATE);
        
        jim.setFriend(mik);
        Call c2 = jim.call(mik);
        mik.pickup(c2);
        wait(1);
        mik.hangup(c2);
        
        assertTrue(jim.totalCharge == LOCAL_RATE);
        assertTrue(mik.totalCharge == 0);
        assertTrue(jim.totalConnectTime == 2);
        assertTrue(mik.totalConnectTime == 2);		
	}

	@Test
	public void testConferenceCall() {
		Call c1 = jim.call(mik);
        mik.pickup(c1);
        wait(1);
        Call c2 = jim.call(crista);
        crista.pickup(c2);      
        c1.merge(c2);      
        wait(1);
        jim.hangup(c1);
        assertTrue(jim.totalCharge == 2*LOCAL_RATE + 1*LONG_DISTANCE_RATE);
	}
	
    protected static void wait(int seconds) {
        Object dummy = new Object();
        synchronized (dummy) {
            try {dummy.wait(seconds*1000); }
            catch (Exception e) {}
        }
    }
}
