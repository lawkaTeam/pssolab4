import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EarnFriendTest {
	private Customer jim;
	private Customer mik;
	private Customer crista;
	private Customer eve;
	private static final long LOCAL_RATE = 3;
	private static final long LONG_DISTANCE_RATE = 10;
	
	
	@Before
	public void setUp() throws Exception {
		jim = new Customer("Jim", 650);
    	mik = new Customer("Mik", 650);
    	crista = new Customer("Crista", 415);
    	eve = new Customer("Eve", 415);
	}

	@Test
	public void testCall1() {
        Call c1 = jim.call(crista);
        crista.pickup(c1);
        wait(2);
        jim.hangup(c1);
        assertTrue(jim.totalCharge == 2*LONG_DISTANCE_RATE);
        assertTrue(crista.totalCharge == 0);
        assertTrue(jim.totalConnectTime == 2);
        assertTrue(crista.totalConnectTime == 2);
        assertTrue(crista.freeSeconds == 1);
        
        Call c2 = crista.call(eve);//Crista wykorzysta 1 darmowa sekunde
        eve.pickup(c2);
        wait(2);
        eve.hangup(c2);
        assertTrue(crista.totalCharge == 1*LOCAL_RATE);
        assertTrue(eve.totalCharge == 0);
        assertTrue(crista.totalConnectTime == 4);
        assertTrue(eve.totalConnectTime == 2);
        assertTrue(crista.freeSeconds == 0);
        assertTrue(eve.freeSeconds == 0);        
	}

	@Test
	public void testCall2() {
				
        Call c1 = jim.call(crista);//Crista otrzyma darmowa sekunde za odebranie 2 sekundowej nielokalnej rozmowy
        crista.pickup(c1);
        wait(2);
        jim.hangup(c1);
        assertTrue(jim.totalCharge == 2*LONG_DISTANCE_RATE);
        assertTrue(crista.totalCharge == 0);
        assertTrue(jim.totalConnectTime == 2);
        assertTrue(crista.totalConnectTime == 2);
        assertTrue(crista.freeSeconds == 1);

        Call c2 = crista.call(mik);//Crista nie wykorzysta darmowej sekundy, bo rozmowa nie jest lokalna
        mik.pickup(c2);
        wait(2);
        mik.hangup(c2);
        assertTrue(crista.totalCharge == 2*LONG_DISTANCE_RATE);
        assertTrue(mik.totalCharge == 0);
        assertTrue(crista.totalConnectTime == 4);
        assertTrue(mik.totalConnectTime == 2);
        assertTrue(crista.freeSeconds == 1);
        assertTrue(mik.freeSeconds == 1);  	

        crista.setFriend(eve);
        Call c3 = crista.call(eve);//Crista nie wykorzysta darmowej sekundy, bo rozmowa jest dla niej darmowa
        eve.pickup(c3);
        wait(2);
        eve.hangup(c3);
        assertTrue(crista.totalCharge == 2*LONG_DISTANCE_RATE);
        assertTrue(eve.totalCharge == 0);
        assertTrue(crista.totalConnectTime == 6);
        assertTrue(eve.totalConnectTime == 2);
        assertTrue(crista.freeSeconds == 1);
        assertTrue(eve.freeSeconds == 0);  	
	}
	
	@Test
	public void testConferenceCall() {
		Call c1 = jim.call(mik);
        mik.pickup(c1);
        wait(1);
        Call c2 = jim.call(crista);
        crista.pickup(c2);      
        c1.merge(c2);      
        wait(2);
        jim.hangup(c1);
        assertTrue(jim.totalCharge == 3*LOCAL_RATE + 2*LONG_DISTANCE_RATE);
        assertTrue(crista.freeSeconds == 1);
        assertTrue(mik.freeSeconds == 0); 
	}
	
    protected static void wait(int seconds) {
        Object dummy = new Object();
        synchronized (dummy) {
            try {dummy.wait(seconds*1000); }
            catch (Exception e) {}
        }
    }

}
